FROM ubuntu:20.04

ENV LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    TZ="Europe/Amsterdam"

ARG DEBIAN_FRONTEND=noninteractive

COPY etc/locale.gen /etc/locale.gen

# Mount /tmp and /var/lib/apt/lists as tmpfs to prevent any files in those dirs leaking into the image.
RUN --mount=type=tmpfs,target=/tmp \
    --mount=type=tmpfs,target=/var/lib/apt/lists \
    apt-get --yes --quiet update \
 && apt-get --yes --quiet install --no-install-recommends ca-certificates locales supervisor tzdata \
 && apt-get --yes --quiet clean

CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
